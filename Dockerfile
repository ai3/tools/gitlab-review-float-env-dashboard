FROM golang:1.23 as build
ADD . /src
RUN cd /src && go build -tags netgo -o gitlab-review-float-env-dashboard ./main.go && strip gitlab-review-float-env-dashboard

FROM scratch
COPY --from=build /src/gitlab-review-float-env-dashboard /gitlab-review-float-env-dashboard
CMD ["/gitlab-review-float-env-dashboard"]
