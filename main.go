package main

import (
	"bytes"
	"compress/flate"
	"encoding/base64"
	"encoding/json"
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
)

var (
	addr           = flag.String("addr", ":3499", "address to listen on")
	gitlabURL      = flag.String("gitlab-url", "https://git.autistici.org", "Gitlab public URL")
	title          = flag.String("title", "VMine test cluster", "title string")
	contentTplFile = flag.String("template", "", "custom content template file, use default if empty")
)

var (
	defaultContentTplSrc = `
{{$jumpHost := "miscredenza.investici.org"}}
{{$socksIP := (index .Inventory 0).IP}}

<h4>Hosts</h4>

<p>
  You can connect to individual hosts over SSH via the jump host <i>{{$jumpHost}}</i>.<br>
  Download the required SSH private key here:
  <a href="{{.GitlabURL}}/{{.ProjectName}}/-/jobs/{{.JobID}}/artifacts/file/build-{{.JobID}}/ssh/vmkey">vmkey</a>.
  Remember to <code>chmod 600</code> the private key file after you download it.
</p>

<table class="table">
<tbody>
{{range $index, $host := .Inventory}}
<tr>
  <td>
   <div class="host">
    <b class="name">{{$host.Name}}</b> {{$host.IP}}<br>
    <span class="attrs">
      {{$host.CPU}} cores, {{$host.RAM}}M ram
    </span>
   </div>
  </td>
  <td>
    <pre>ssh -o UserKnownHostsFile=/dev/null -i vmkey -J {{$jumpHost}} root@{{$host.IP}}</pre>
  </td>
</tr>
{{end}}
</tbody>
</table>

<h4>HTTP connection (via SOCKS5 proxy)</h4>

<p>
  Forward a local port to the SOCKS5 proxy running on {{$socksIP}}, and run a
  private browser that uses it:
</p>

<pre>ssh -nN -L 9051:{{$socksIP}}:9051 {{$jumpHost}} &
chromium --user-data-dir=$(mktemp -d) --no-first-run --no-default-browser-check \
  --proxy-server=socks5://localhost:9051 https://...
</pre>
`
	defaultContentTpl = template.Must(template.New("").Parse(defaultContentTplSrc))

	dashTplSrc = `<!doctype html>
<html lang="en">
<head>
  <title>{{.Title}}</title>
  <style type="text/css">
body { background: white; }
.name { font-size: 120%; }
.attrs { color: #666; font-size: 90%; }
.table { border: 0; width: 100%; }
.host {
  padding-left: 40px;
  background: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSIyNHB4IiBoZWlnaHQ9IjI0cHgiIHN0cm9rZS13aWR0aD0iMS41IiB2aWV3Qm94PSIwIDAgMjQgMjQiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgY29sb3I9IiMwMDAwMDAiPjxwYXRoIGQ9Ik02IDE4LjAxbC4wMS0uMDExTTYgNi4wMWwuMDEtLjAxMSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjEuNSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIj48L3BhdGg+PHBhdGggZD0iTTIgOS40VjIuNmEuNi42IDAgMDEuNi0uNmgxOC44YS42LjYgMCAwMS42LjZ2Ni44YS42LjYgMCAwMS0uNi42SDIuNmEuNi42IDAgMDEtLjYtLjZ6TTIgMjEuNHYtNi44YS42LjYgMCAwMS42LS42aDE4LjhhLjYuNiAwIDAxLjYuNnY2LjhhLjYuNiAwIDAxLS42LjZIMi42YS42LjYgMCAwMS0uNi0uNnoiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLXdpZHRoPSIxLjUiPjwvcGF0aD48L3N2Zz4K") no-repeat left 5px;
}
  </style>
</head>
<body>

  <h1>{{.Title}}</h1>

  {{.Content}}

</body>
</html>`

	dashTpl = template.Must(template.New("").Parse(dashTplSrc))

	contentTpl *template.Template
)

func parseTemplate() error {
	if *contentTplFile == "" {
		contentTpl = defaultContentTpl
		return nil
	}

	data, err := os.ReadFile(*contentTplFile)
	if err != nil {
		return err
	}
	contentTpl, err = template.New("").Parse(string(data))
	return err
}

type hostinfo struct {
	Name  string  `json:"name"`
	Image string  `json:"image"`
	CPU   float64 `json:"cpu"`
	RAM   float64 `json:"ram"`
	IP    string  `json:"ip"`
}

type payload struct {
	Inventory   []*hostinfo `json:"inv"`
	JobID       string      `json:"job"`
	ProjectName string      `json:"proj"`
}

func decompress(input []byte) ([]byte, error) {
	var out bytes.Buffer
	r := flate.NewReader(bytes.NewReader(input))
	defer r.Close()
	_, err := io.Copy(&out, r)
	return out.Bytes(), err
}

func decodePayload(encoded string) (*payload, error) {
	compressed, err := base64.URLEncoding.DecodeString(encoded)
	if err != nil {
		return nil, err
	}

	decompressed, err := decompress(compressed)
	if err != nil {
		return nil, err
	}

	var p payload
	if err := json.Unmarshal(decompressed, &p); err != nil {
		return nil, err
	}

	return &p, err
}

func handleRequest(w http.ResponseWriter, req *http.Request) {
	payload, err := decodePayload(req.URL.Path)
	if err != nil {
		log.Printf("error decoding payload: %v", err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "no-store")

	// Render internal template.
	var buf bytes.Buffer
	if err := contentTpl.Execute(&buf, struct {
		Inventory   []*hostinfo
		ProjectName string
		JobID       string
		Title       string
		GitlabURL   string
	}{
		Inventory:   payload.Inventory,
		ProjectName: payload.ProjectName,
		JobID:       payload.JobID,
		Title:       *title,
		GitlabURL:   *gitlabURL,
	}); err != nil {
		log.Printf("template error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// nolint: errcheck
	dashTpl.Execute(w, struct {
		Title   string
		Content template.HTML
	}{
		Title:   *title,
		Content: template.HTML(buf.String()),
	})
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if err := parseTemplate(); err != nil {
		log.Fatalf("template error: %v", err)
	}

	http.Handle("/dash/", http.StripPrefix("/dash/", http.HandlerFunc(handleRequest)))

	log.Fatal(http.ListenAndServe(*addr, nil))
}
